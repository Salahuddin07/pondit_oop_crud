<?php

class DB{

    public $host='localhost';
    public $user='root';
    public $db='php_oop_crud';
    public $password = '';
    public $connection;



    public function __construct()
    {
        $dsn = "mysql:host=$this->host;dbname=$this->db;charset=UTF8";
        $pdo = new PDO($dsn, $this->user, $this->password);

	    if ($pdo) {
		     $this->connection = $pdo;
	    }
    }
}


?>