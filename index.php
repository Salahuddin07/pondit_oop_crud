<?php

include './Student.php';

$st = new Student;
$students = $st->index();

session_start();

?>



<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
</head>

<body>

    <div class="container">
    <?php
                if(isset($_SESSION['msg'])){
                    $msg = $_SESSION['msg'];
                    echo " <p class='alert alert-success' >$msg</p> ";
                    unset($_SESSION['msg']);
                }
            ?>

        <div class="card">
            
            <div class="card-header">
                Student list
                <a href="./create.php" class="btn btn-sm btn-primary">Add new Student</a>
            </div>
            <div class="card-body p-4">
                <table class="table table-sm table-bordered">
                    <thead>
                        <?php $count = 1; ?>
                        <tr class="table-dark text-white">
                            <th scope="col">Serial no.</th>
                            <th scope="col">Name</th>
                            <th scope="col">Actions</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($students as $student) : ?>
                            <tr>
                                <th scope="row"><?= $count++ ?></th>
                                <td><?= $student['name'] ?></td>

                                <td>
                                    <a href="edit.php?std_id=<?=$student['id']?>" class="btn btn-sm btn-warning">Edit</a>
                                    <a href="" class="btn btn-sm btn-danger">Delete</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
</body>

</html>